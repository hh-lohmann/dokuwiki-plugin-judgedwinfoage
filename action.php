<?php
    if(!defined('DOKU_INC')) die();
    if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
    require_once DOKU_PLUGIN.'action.php';
 
    class action_plugin_judgeDwInfoAge extends DokuWiki_Action_Plugin {
 
        /**
         * Register its handlers with the DokuWiki's event controller
         */
        function register(&$controller) {
            $controller->register_hook('TPL_ACT_RENDER', 'BEFORE', $this, '_judgeDwInfoAge');
        }
 
        function _judgeDwInfoAge(){
                global $ACT;
                if ( $ACT == 'show' )  {
                    global $INFO;
                    $type = 'note';
                    $size = '0.9em';
                    $color = 'gray';
                    $weight = 'normal';
                    $float = 'none';
                    if ( isset( $INFO['editor'] ) )  {
                        $pagedate = $INFO['lastmod'] ;
                        $msg = '<i>Letzte &Auml;nderung '.dformat($pagedate,'%d.%m.%Y %H:%M Uhr').' durch '.editorinfo($INFO['editor']).'</i>';
                    }
                    else {
                          $pagedate = filemtime( $INFO['filepath'] ) ;
                          $msg = '<span style="color: red;">Keine Änderungsinformationen verfügbar (Externe Bearbeitung? Dateikopie zwischen verschiedenen DokuWiki-Instanzen?)</span><br>Dateidatum der Seite: '.strftime('%d.%m.%y %H:%M', $pagedate );
                    }
                    if ( ( time ( ) - $pagedate ) > 64000000 ) {
                        $type = 'important';
                        $size = '1.5em';
                        $color = 'red';
                        $weight = 'bold';
                        $float = 'left';
                        $msg = 'Achtung: Seite ist &auml;lter als zwei Jahre - m&ouml;glicherweise nicht mehr aktuell?<br/>'.$msg;
                    }
                    echo '<div style=" font-size: '.$size.'; color: '.$color.'; font-weight: '.$weight.'; "><img src="/doku/lib/plugins/note/images/'.$type.'.png" style=" margin-right: 1em; float: '.$float.'; " />'.$msg.'</div>';
                }
        }
    }
?>
